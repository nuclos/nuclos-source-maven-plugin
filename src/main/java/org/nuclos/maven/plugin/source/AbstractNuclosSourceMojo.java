package org.nuclos.maven.plugin.source;

import java.io.File;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 * Base class for Nuclos source plugin Mojos that are accessing Nuclet content
 * <p>
 * This abstract class does handle some of the very common stuff for interacting with a Nuclet on a file system like
 * verification or setting up DOM handling objects.
 * <p>
 * Inherited classes will override the {@link AbstractNuclosSourceMojo#run()} method which get's called during the usual
 * Mojo's execution.
 */
abstract class AbstractNuclosSourceMojo extends AbstractMojo {

    /**
     * A list of paths that contain valid Nuclets and are going to be searched for content
     */
    @Parameter(required = true)
    List<File> nuclets;

    @Parameter(readonly = true, defaultValue = "${project}")
    MavenProject project;

    /**
     * Defines the directory where the plugin should write the results to
     * <p>
     * Note that there will be sub-directories created for the different goals that can be invoked
     */
    @Parameter(defaultValue = "${project.build.directory}/generated-sources/nuclos-sources")
    private File outputDirectory;

    /**
     * Instructs the plugin to fail the build if any given Nuclet location is invalid
     */
    @Parameter(defaultValue = "false")
    private boolean failIfNucletInvalid = false;

    /**
     * Skips the whole plugin execution
     */
    @Parameter(defaultValue = "false", property = "nuclos.source.skip")
    private boolean skip = false;

    DocumentBuilder documentBuilder;

    XPath xPath;

    final private static Pattern WILDCARD_PATTERN = Pattern.compile("[^*]+|(\\*)");

    @Override
    final public void execute() throws MojoExecutionException, MojoFailureException {
        if (skip) {
            getLog().info("Skipping execution because of 'skip' option");
            return;
        }

        for (File nuclet : Optional.ofNullable(nuclets).orElse(Collections.emptyList())) {
            if (!isNucletLocationValid(nuclet.toPath())) {
                String message = "Directory " + nuclet + " does not represent a valid Nuclet location";
                if (failIfNucletInvalid) {
                    throw new MojoFailureException(message);
                } else {
                    getLog().warn(message);
                }
            }
        }

        try {
            documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new MojoFailureException("Could not setup DOM parser");
        }
        xPath = XPathFactory.newInstance().newXPath();

        run();
    }

    /**
     * Method where the actual Mojo implementation is made
     *
     * @throws MojoExecutionException
     * @throws MojoFailureException
     */
    abstract protected void run() throws MojoExecutionException, MojoFailureException;

    final protected File getNuclosSourceOutputDirectory() {
        return outputDirectory;
    }

    /**
     * Checks if a given location references a valid Nuclet
     * <p>
     * A Nuclet location is assumed to be valid if it is a directory, can be accessed and does contain a file called
     * <code>nuclet.xml</code>
     *
     * @param nucletLocation the {@link Path} to the Nuclet location
     * @return <code>true</code> if <code>nucletLocation</code> references a readable directory and contains a file
     * <code>nuclet.xml</code>, <code>false</code> otherwise
     */
    boolean isNucletLocationValid(Path nucletLocation) {
        File nuclet = nucletLocation.toFile();
        return nuclet.exists() && nuclet.isDirectory() && nuclet.canRead()
                && nucletLocation.resolve("nuclet.xml").toFile().exists();
    }

    void addTemporarySourceDirectory(Path dir) {
        this.project.addCompileSourceRoot(dir.toString());

        if (getLog().isInfoEnabled()) {
            getLog().info("Source directory: " + dir + " added.");
        }
    }

    Set<Pattern> compileFilters(Set<String> filters) {
        return filters.stream().map(filter -> {
            Matcher m = WILDCARD_PATTERN.matcher(filter);
            StringBuffer b = new StringBuffer();
            while (m.find()) {
                if(m.group(1) != null) {
                    m.appendReplacement(b, ".*");
                } else {
                    m.appendReplacement(b, "\\\\Q" + m.group(0) + "\\\\E");
                }
            }
            m.appendTail(b);
            return Pattern.compile(b.toString());
        }).collect(Collectors.toSet());
    }

    boolean matchesFilter(String subject, Set<Pattern> filters) {
        return filters.stream().anyMatch(pattern -> pattern.matcher(subject).matches());
    }

}
